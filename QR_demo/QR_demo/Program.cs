﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QRTool
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            args = PreAnalyseParams(args);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DependentFiles.LoadResourceDll();       // 载入资源dll文件

            if (args != null && args.Length > 0)    // 生成参数对应二维码
            {
                foreach (String arg in args)
                {
                    Bitmap pic = Form_QR.ToQR(arg); // 生成二维码
                    Form_QR.Save(pic);              // 保存图像
                }
            }
            else Application.Run(new Form_QR());
        }

        /// <summary>
        /// 参数预解析
        /// </summary>
        static String[] PreAnalyseParams(String[] args)
        {
            List<String> list = new List<string>();

            if (args != null && args.Length > 0)    // 生成参数对应二维码
            {
                foreach (String arg0 in args)
                {
                    String arg = arg0.Trim();
                    if (arg.StartsWith("RENAME=")) Form_QR.RENAME = arg.Substring("RENAME=".Length);
                    else list.Add(arg);
                }
            }

            return list.ToArray();
        }
    }
}
